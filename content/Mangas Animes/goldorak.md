Title: GOLDORAK au Salon de Japan Expo 2018
Date: 2018-08-07 13:29
Tags: Robot, Mangas, Goldorak, 1978, Actarus, OVNI, UFO 
Category: Mangas Animes
Authors: Christophe d'iRTA
Summary: La Japan Expo est le rendez vous des fans de l’univers Japonais. Un incontournable pour nos EXPLOrateurs de Mangas et de Cosplay ! Et en particulier en Robotique avec Goldorak, Le premier Robot Mangas, mis a l’honneur cette année puisque le premier épisode a été diffusé le 3 Juillet 1978 en France il y a donc 40 ans.
Image: images/mangas_animes/goldorak_2018/thumbnail_goldorak.jpg
Status: published

<div class="row">
    <div class="col s12 m4 l9">
<b>La Japan Expo est le rendez vous des fans de l’univers Japonais.</b>  <br>
La Japan Expo est le rendez vous des fans de l’univers Japonais. Un incontournable pour nos EXPLOrateurs de Mangas et de Cosplay ! Et en particulier en Robotique avec Goldorak, Le premier Robot Mangas, mis a l’honneur cette année puisque le premier épisode a été diffusé le 3 Juillet 1978 en France il y a donc 40 ans.
    </div>
    <div class="col s12 m4 l3">
        <center>
            <img class="responsive-img" src="images/mangas_animes/goldorak_2018/goldorak_pose.PNG" alt="Goldorak en pose">
        </center>
    </div>
</div>


<b>Mais qui est donc Goldorak ?</b>

Goldorak est une série animée japonaise produite au Japon diffusée en France sur Antenne2
(France 2 désormais) à partir de 1978 dans l’émission RécréA2. Goldorak en Japonais « Robot
OVNI » est donc un vaisseau spatial équipé d’un robot Géant Piloté par Actarus Prince
héritier de la planète Euphor.


<br>
<br>

<div class="row">
    <div class="col s12 m4 l3">
        <center>
            <img class="responsive-img" src="images/mangas_animes/goldorak_2018/goldorak.PNG" alt="Goldorak">
        </center>
    </div>
    <div class="col s12 m4 l8">
        <b>Quelle est l’histoire de cette série animé ?</b>
        <br>
        Goldorak est un robot de combat qui a été construit par
        l’empire maléfique de Véga et a servi à détruire la planète
        Euphor. Le prince Actarus a réussi à s’en accaparer et
        fuir de sa planète juste a temps et ainsi se réfugier
        sur Terre. Avec l’aide d’ami(e)s ils vont s’unir afin
        de lutter contre l’invasion de l’empire Véga…
    </div>
    <div class="col s12 m4 l1">
        <center>
            <img class="responsive-img" id="personnage_goldorak" src="images/mangas_animes/goldorak_2018/personnage.PNG" alt="Personnage de Goldorak">
        </center>
    </div>
</div>

<b>Et la Japan Expo alors ?</b>

Pour ce 40eme anniversaire on retrouve notre Goldorak chez Jungle (Entertainement Hobby
Shop-D196) et Tamashi Nation (G196) sous forme de figurines plus ou moins grandes et aux
couleurs et formes vraiment top ! La qualité est donc au rendez vous et le rendu est tout
simplement magnifique. Evidemment ce n’est pas un jouet et pourtant on aurait qu’une
envie c’est de le voir voler en vrai comme dans « Toy Story » et pas qu’en l’absence d’être
humain. ;-) Par contre il vous faudra débourser au minimum 100€ pour le modele de base et
si vous avez de la place et surtout économisé vous pouvez opter pour le modele Géant de
Goldorak à 549€. C’est cher mais la passion n’a pas de limite et très peu de figurines sont à
disposition. Elles deviennent donc vite des collectors. Le plaisir de les voir en vrai est
immense d’autant plus quand on est passionné par cette série animé. Pour celles et ceux qui
souhaitent découvrir ou redécouvrir [la série Culte Goldorak](https://www.youtube.com/watch?v=dMMrRPgC0Sk).


<center>
    <img class="responsive-img" src="images/mangas_animes/goldorak_2018/figurines.PNG" alt="Image de différentes figurines de Goldorak">
</center>


Sources

[https://fr.wikipedia.org/wiki/Goldorak](https://fr.wikipedia.org/wiki/Goldorak)

[https://www.japan-expo-paris.com/fr/actualites/goldorak-debarque-a-paris_104322.htm](https://www.japan-expo-paris.com/fr/actualites/goldorak-debarque-a-paris_104322.htm)

[https://culturebox.francetvinfo.fr/livres/bande-dessinee/japan-expo-francois-baranger-a-repondu-al-appel-de-cthulhu-de-lovecraft-276083](https://culturebox.francetvinfo.fr/livres/bande-dessinee/japan-expo-francois-baranger-a-repondu-al-appel-de-cthulhu-de-lovecraft-276083)

[https://www.franceinter.fr/culture/il-y-a-40-ans-goldorak-arrivait-en-france](https://www.franceinter.fr/culture/il-y-a-40-ans-goldorak-arrivait-en-france)


[http://www.fulguropop.com/japan-expo-stand-tamashii-nations-france/](http://www.fulguropop.com/japan-expo-stand-tamashii-nations-france/)

[http://www.mangas.fr/Personnages/rigel-189/263](http://www.fulguropop.com/japan-expo-stand-tamashii-nations-france/)

[http://www.imagespourtoi.com/image/726.html](http://www.imagespourtoi.com/image/726.html)