Title: #1 Infos Dev Sondage&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Date: 2018-08-17 12:42
Tags: infos, développement, infos dev, 
Category: Infos Dev
Authors: Victor Lohézic
Summary: Le site est sorti ! Les Infos Dev c'est quoi ? 
Image: images/infos_dev/infos_dev1/thumbnail.png
State: publié

Les <b>Infos Dev</b> sont des articles qui expliquent les dernières mises à jour, les futures améliorations du site web. Elles sont écrites par les développeurs d'<b>iRoboTechArt.com</b>. 
<br>
<br>

<center>
    <img class="responsive-img" src="https://img1.lght.pics/2O4Q.png" alt="Logo iRoboTechArt">
</center>

Tout d'abord, nous sommes heureux d'annoncer la sortie du site web officiel de l'association <b>iRoboTechArt</b>. Sur ce site, vous pouvez trouver tous les projets de l'association, son actualité mais celle à l'échelle mondiale dans différents thèmes de la robotique, du manga, de la technolgie, des jeux vidéos...

<br>
<br>

Cette première version peut cependant comporter des bugs, des erreurs, qui nous ont échappés. Afin de les régler le plus rapidement, une adresse mail a été mise en place pour que vous puissiez nous contacter : <b>webmasters@irobotechart.com</b> N'hésitez pas à nous les faire parvenir car le site appartient avant tout à ses utilisateurs. En revanche, si vous voulez contacter l'association à la place de l'équipe de développement du site, préférez cette adresse mail : contact@irobotechart.com
<br>
<br>
iRoboTechArt est un site OpenSource où le code est acessible sur la plateforme GitLab. C'est une plateforme sur laquelle on peut déposer le code d'un projet, permettant la contribution privée, restreinte, ou publique d'utilisateurs. Elle repose sur un modèle OpenCore. Nous vous la présenterons plus en détail sûrement dans un prochain article. En attendant, n'hésitez pas à télécharger le site en local sur votre ordinateur : [https://gitlab.com/iRoboTechArt/website](https://gitlab.com/iRoboTechArt/website). Le site utilise pour technologie un générateur de sites statiques : [Pélican](http://docs.getpelican.com/en/stable/) écrit en [python](https://www.python.org/). 
<div class="row">
    <div class="col m6 s6 l5 offset-l2">
        <img class="responsive-img" src="images/infos_dev/infos_dev1/gitlab_logo.png" width="50%" alt="Logo GitLab">
    </div>
    <div class="col m6 s6 l5">
        <img class="responsive-img" src="images/infos_dev/infos_dev1/pelican_logo.png" width="50%" alt="Logo Pelican">
    </div>
</div>


Le site est donc toujours en développement, en effet dans la barre de menu, vous pouvez y repérer l'onglet projet. Mais les pages des projets ne sont pas encore disponibles. Ainsi le lien ne redirige sur aucune page. 
<br>
<br>


Cette première version n'a pas encore de nom. Vous connaissez surement les noms de dessert des versions Android. Pour iRoboTechArt, on vous laisse proposer des thèmes, répondez au formulaire ci-dessous : 
<br>
<br>
<br>
<br>


<iframe src="https://framaforms.org/nom-des-versions-1533646177" width="100%" height="800" border="0" ></iframe>  

Bon surf sur iRoboTechArt, votre ordinateur n'a jamais été aussi préparé ;)

<center>
    <img class="responsive-img" src="images/infos_dev/infos_dev1/ordinateur.png" width="75%" alt="Ordinateur musclé">
</center>


