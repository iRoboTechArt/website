Title: #2 Infos Dev Rires et Blagues
Date: 2018-08-25 12:30
Tags: infos, développement, infos dev, 
Category: Infos Dev
Authors: Victor Lohézic
Summary: Une nouvelle version du site vient de sortir, découvrez les changements. 
Image: images/infos_dev/infos_dev2/thumbnail.png
State: publié

Les <b>Infos Dev</b> sont des articles qui expliquent les dernières mises à jour, les futures améliorations du site web. Elles sont écrites par les développeurs d'<b>iRoboTechArt.com</b>. 
<br>
<br>
<center>
    <img class="responsive-img" src="https://img1.lght.pics/2O4Q.png" alt="Logo iRoboTechArt">
</center>

Bienvenue sur la nouvelle version du site. Nous allons vous présenter les modifications. 

### Page d'accueil 

Vous pouvez désormais lire le résumé, l'accroche de chaque article avant de le lire. Vous pouvez aussi cliquer sur l'auteur de l'article pour voir toutes ses autres publications. 

### Catégories d'articles

Pour accéder aux différentes catégories d'articles publiées sur le site, il suffit de cliquer sur Toute L'Actu. Ainsi, l'affichage des articles d'une catégorie a été modifié. En effet, celui-ci est identique à la page d'accueil et présente donc les mêmes fonctionnalités. 

### Générateur de blagues aléatoires

L'image derrière la blague sur la page d'accueil a été éclaircie pour faciliter la lecture. Un générateur de blagues aléatoires a été ajouté. N'hésitez pas à envoyer vos meilleurs blagues à l'adresse suivante : <b>webmasters@irobotechart.com</b>

### Contribution 

Vous voulez contribuer au développement du site, aucun souci. Il existe différentes manières. On peut participer à la production de lignes de code : [https://gitlab.com/iRoboTechArt/website](https://gitlab.com/iRoboTechArt/website) ou d'images, l'écriture de blagues ou d'articles. Vous pouvez aussi donner un thème pour nommer les versions du site : 

<iframe src="https://framaforms.org/nom-des-versions-1533646177" width="100%" height="800" border="0" ></iframe>  

Maintenant, nous vous souhaitons une bonne visite sur le site !
