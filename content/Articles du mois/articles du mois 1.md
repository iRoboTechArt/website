Title: #1 Articles du mois
Date: 2018-11-03 22:30
Tags:  
Category: Articles du mois  
Authors: Victor Lohézic
Summary: Quels sont les articles les plus visités durant le mois d'octobre ? 
Image: images/articles_du_mois/articles_du_mois1/thumbnail.png
Status: published

Voici un graphique et un tableau qui montrent les articles les plus consultés durant le mois d'octobre sur le site :

<center>
    <img class="responsive-img" src="images/articles_du_mois/articles_du_mois1/chart.png" alt="Graphique avec les articles les plus consultés">
</center>
<br>
<br>
     <table class="responsive-table">
        <thead>
          <tr>
              <th>Nom de l'article</th>
              <th>Pourcentage du nombre de visites</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>[Halloween avec OpenScad](halloween-avec-openscad.html)</td>
            <td>35%</td>
          </tr>
          <tr>
            <td>[Enigme avec des lapins](enigme-avec-des-lapins.html
)</td>
            <td>28%</td>
          </tr>
          <tr>
            <td>[OpenScad un logiciel de modélisation 3D](openscad-un-logiciel-de-modelisation-3d.html
)</td>
            <td>12%</td>
          </tr>
          <tr>
            <td>[Goldorak au salon de la Japan Expo 2018](goldorak-au-salon-de-japan-expo-2018.html
)</td>
            <td>11%</td>
          </tr>
          <tr>
            <td>[#1 Infos Dev Sondage](1-infos-dev-sondage.html
)</td>
            <td>3%</td>
          </tr>
          <tr>
            <td>[Un Robot Chat OpenSource](un-robot-chat-opensource.html
)</td>
            <td>3%</td>
          </tr>
          <tr>
            <td>[La Gamebuino une Gameboy Arduino](la-gamebuino-une-gameboy-arduino.html
)</td>
            <td>2%</td>
          </tr>
          <tr>
            <td>[#2 Infos Dev Rires et Blague](2-infos-dev-rires-et-blagues.html
)</td>
            <td>2%</td>
          </tr>
          <tr>
            <td>[Donnez vie à vos LEGO](donnez-vie-a-vos-lego.html
)</td>
            <td>2%</td>
          </tr>
        </tbody>
      </table>