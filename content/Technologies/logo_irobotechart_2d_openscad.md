Title: OpenScad, création du logo d'iRoboTechArt
Date: 2018-11-22 13:09
Tags: logiciel, CAO, 3D, imprimante 3D, iRoboTechArt
Category: Technologies
Authors: Victor Lohézic
Summary: Vous aimeriez apprendre à utiliser OpenScad, mais vous ne savez pas par où commencer. Et si nous créons ensemble le logo d'iRoboTechArt.
Image: images/technologies/irobotechart_logo_openscad/logo.png
Status: published

<script>

  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
    </script>
    
<b>Vous aimeriez apprendre à utiliser OpenScad, mais vous ne savez pas par où commencer.</b> Et si nous créons ensemble le logo d'iRoboTechArt. Vous ne connaissez pas le logiciel OpenScad, allez voir cet [article](openscad-un-logiciel-de-modelisation-3d.html).
<center>
    <img class="responsive-img" src="images/technologies/irobotechart_logo_openscad/logo.png" alt="Logo iRoboTechArt">
</center>
Cet article sera composé de petites missions. Essayer d'y répondre sans regarder les solutions. [Cette feuille](http://www.openscad.org/cheatsheet/index.html) sera votre plus grande alliée alors ne vous en privé pas ;) 
  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel teal">
          <span class="card-title white-text"><b>Mission 1</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Créer un cylindre avec 7mm de hauteur et 50mm de diamètre (les dimensions s'expriment en mm dans OpenScad).
            <center><br><img class="responsive-img" width="50%" src="images/technologies/irobotechart_logo_openscad/cylinder.png" alt="Image cylindre"></center>
        </span>
      </div>
    </div>
  </div>
   <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body"><span><code>cylinder(7,d=50,$fn=100);</code>
          <br>
          <i>$fn permet d'indiquer la résolution</i> 
          </span></div>
    </li>
  </ul>
    <div class="row">
    <div class="col s12 m12">
      <div class="card-panel teal">
          <span class="card-title white-text"><b>Mission 2</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Ajouter une rainure de 35mm de longueur, 5mm de largeur et 3mm de hauteur.
            <center><br><img class="responsive-img" width="50%" src="images/technologies/irobotechart_logo_openscad/rainure1.png" alt="Image avec la rainure"></center>
        </span>
      </div>
    </div>
  </div>
   <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body"><span>
                                  <code>
                                  difference(){<br>
                                        //base cylindre<br>
                                        cylinder(7,d=50,$fn=100);<br>
                                        //rainure<br>
                                        translate([0,0,6]){<br>
                                            cube([5,35,3], center=true);<br>
                                        }<br>
                                    }<br>
                                  </code>
          <br>
          <br>
          Le <b>cube</b> représente la matière qui va être enlevée. La fonction <b>difference</b> permet l'enlèvement de matière pour faire la rainure. La fonction <b>translate</b> permet de déplacer le cube sur les axes [x,y,z]. Les <b>//</b> permettent d'écrire des commentaires qui ne seront pas exécutés par le compilateur dans le but de rendre le code plus clair.
          </span></div>
    </li>
  </ul>
    <div class="row">
    <div class="col s12 m12">
      <div class="card-panel teal">
          <span class="card-title white-text"><b>Mission 3</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Ajouter deux rainures de 50mm de longueur, 4mm de largeur et 3mm de hauteur.
            <center><br><img class="responsive-img" width="50%" src="images/technologies/irobotechart_logo_openscad/rainure2.png" alt="Image du logo en 3D"></center>
        </span>
      </div>
    </div>
  </div>
   <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body"><span>
          <code>difference(){
                    //base cylindre<br>
                    cylinder(7,d=50,$fn=100);<br>
                    //rainure<br>
                    translate([0,0,6]){<br>
                        cube([5,35,3], center=true);<br>
                    }
                    //rainure 1<br>
                    rotate([0,0,90]){<br>
                        translate([17.5,0,6]){<br>
                            cube([4,50,3], center=true);<br>
                        }<br>
                    }<br>
                    //rainure 2<br>
                    rotate([0,0,90]){<br>
                        translate([-17.5,0,6]){<br>
                            cube([4,50,3], center=true);<br>
                        }<br>
                    }<br>
                }<br>
          </code>
          <br>
          <br>
          La fonction <b>rotate</b> permet de tourner le cube sur les axes [x,y,z].
          </span></div>
    </li>
  </ul>
  
  Bravo, maintenant, il ne vous reste plus qu'à exporter dans le format STL et à imprimer. 



