Title: Donnez vie à vos LEGO
Date: 2018-07-17 12:42
Tags: LEGO, arduino, électricité, électronique, moteur
Category: Technologies
Authors: Victor Lohézic
Summary: Les LEGO permettent de laisser libre cours à notre imagination pour des créations uniques. Celles-ci sont souvent statiques, n'aimeriez vous pas que la voiture de vos rêves puissent rouler ? 
Image: images/technologies/donner_vie_a_vos_lego/image.jpg
Status: published

Les LEGO permettent de laisser libre cours à notre imagination pour des créations uniques. Celles-ci sont souvent statiques, n'aimeriez-vous pas que la voiture de vos rêves puisse rouler ? 

<center>
    <img class="responsive-img" src="images/technologies/donner_vie_a_vos_lego/motor.jpg" alt="Image moteur">
</center>

Jason Kirsons est un autralien. Il en avait assez d'utiliser dans ses projets des moteurs fournis par LEGO. Ainsi en juin 2018, il proposa un tutorial afin d'utiliser d'autres moteurs plus petits et moins chers. Son tutorial est facile d'accès cependant si vous rencontrez des difficultés, n'hésitez pas à venir demander de l'aide aux LABS. Il a traduit les sous-titres de sa vidéo en français afin de vous faciliter la compréhension. 

<div class="video-container">
    <iframe width="615" height="352" src="https://www.youtube.com/embed/-yAbtxD6Sa4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

Pour que le moteur s'adapte à la construction en LEGO, Jason a modélisé en 3D des pièces grâce au logiciel OpenScad.

Le boitier du moteur est en deux parties :

<center>
    <div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/eb647db9dad841d2aa4ea361bfd20b55/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/eb647db9dad841d2aa4ea361bfd20b55?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">n20_motor_mount_v2_vPsTO7LBov</a>
    by <a href="https://sketchfab.com/benjamin_l?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Hackster.io</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>

<br>

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/dacf9e3ea68c4061888a93c2fd214c00/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/dacf9e3ea68c4061888a93c2fd214c00?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">n20_motor_mount_v2_-_p2_SPMMK5zaKE</a>
    by <a href="https://sketchfab.com/benjamin_l?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Hackster.io</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
</center>

Un adapteur aux pièces LEGO :

<center>
        <div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/c4dcdee378fb42d08fd3035acaab05d7/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/c4dcdee378fb42d08fd3035acaab05d7?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">axle_adapter_QQnC6cf50A</a>
    by <a href="https://sketchfab.com/benjamin_l?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Hackster.io</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
</center>

Afin de mieux comprendre le projet de Jason Krisons, je lui ai posé quelques questions. J'ai traduit ses réponses que pouvez découvrir ci-dessous : 

<div class="video-container">
    <iframe width="640" height="360" src="https://www.youtube.com/embed/L7yKFgXCZoI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

Ce projet est très intéressant pour un débutant. Celui-ci allie création, électronique et mécanique. Il permet donc de s'initier à différents domaines des sciences de l'ingénieur pour s'engager ensuite dans des projets plus importants. 

<br>

En savoir plus : 

[Le FabLAB et son imprimante 3D](labs/fablab.html)

[Tutorial de Jason Kirsons](https://www.hackster.io/gadget-workbench/use-cheap-motors-with-lego-04a6b5)

[OpenScad](http://www.openscad.org/)



<p>
    Toutes les images et vidéos utilisées dans cet article ont reçues l'autorisation de Jason Kirsons.
</p>





