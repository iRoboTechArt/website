Title: Piskel, un éditeur de sprites
Date: 2019-01-26 18:22
Tags: piskel, iRoboTechArt, sprite, jeux vidéos
Category: Technologies
Authors: Victor Lohézic
Summary: Dans cet article, découvrez Piskel un outil gratuit et libre pour créer ses sprites en Pixel Art.
Image: images/technologies/piskel_un_editeur_de_sprites/thumbnail.png
Status: published

<b>Piskel est un éditeur de [sprites](https://fr.wikipedia.org/wiki/Sprite_(jeu_vid%C3%A9o)) en [Pixel Art](Pixel art).</b>
<br>
<center>
    <img class="responsive-img" src="images/technologies/piskel_un_editeur_de_sprites/piskel.png" alt="Logo Piskel" width="50%">
</center>
<br>
Il dispose d'une [version en ligne](https://www.piskelapp.com/) et d'une [version hors-ligne](https://www.piskelapp.com/download) disponible sur <b>Windows, Mac Os, [Gnu/Linux](https://www.gnu.org/gnu/why-gnu-linux.fr.html)</b>. Pour l'instant, il n'existe pas de version mobile. Piskel est sous [licence libre](https://www.gnu.org/licenses/license-list.fr.html#apache2). Il est accessible sur [GitHub](http://github.com/piskelapp/piskel). Son code est essentiellemnt constitué d'<b>[HTML](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language)</b>, de<b> [CSS](https://fr.wikipedia.org/wiki/Feuilles_de_style_en_cascade)</b> et <b>[Javascript](https://fr.wikipedia.org/wiki/JavaScript)</b>.
<br>
Dans la version en ligne, pour conserver ses créations, il est nécessaire de se connecter avec <b>son compte Google</b>. Il est possible de choisir la visibilité de ses réalisations (public ou privé).
<br>
<br>
<center>
    <img class="responsive-img" src="images/technologies/piskel_un_editeur_de_sprites/piskel_interface.png" alt="Interface de Piskel" width="50%">
</center>
<br>
L'interface pour créer ses images est très agréable. Elle est très intuitive et la prise en main est plaisante. Elle permet aussi de créer des animations. Cet outil est très intéressant pour créer les personnages, les <b>ressources d'un jeu vidéo</b>. On peut ensuite exporter les images en <b>PNG</b>, en <b>GIF</b>...
<br>
<center>
    <img class="responsive-img" src="images/technologies/piskel_un_editeur_de_sprites/export.gif" alt="Interface de Piskel" width="50%">
</center>    

Pour vous entraîner, pourquoi ne pas essayer de reproduire le <b>logo d'iRoboTechArt</b> ? 

<center>
    <img class="responsive-img" src="images/technologies/piskel_un_editeur_de_sprites/logo.png" alt="Interface de Piskel" width="50%">
</center>    