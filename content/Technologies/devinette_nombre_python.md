Title: Programmer un petit jeu : Deviner Un Nombre En Python
Date: 2019-01-11 12:22
Tags: python, iRoboTechArt
Category: Technologies
Authors: Victor Lohézic
Summary: Amusez-vous à programmer un petit jeu dans lequel l'utilisateur doit deviner un nombre.
Image: images/technologies/jeu_devinette_nombre_python/thumbnail.png
Status: published

<script>

  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
    </script>

<center>
    <img class="responsive-img" src="images/technologies/jeu_devinette_nombre_python/thumbnail.png" width="50%" alt="Image d'intro">
</center>
<br>
<br>
<br>
<b>Amusez-vous à programmer un petit jeu dans lequel l'utilisateur doit deviner un nombre. </b>On propose à l'utilisateur de rentrer un nombre compris entre 0 et 100. Il rentre un nombre. Il y a trois possibilités :
<ol>
    <li>le joueur a rentré un nombre inférieur à celui demandé et on le lui dit</li>
    <li>le joueur a rentré un nombre supérieur à celui demandé et on le lui dit</li>
    <li>le joueur a trouvé le bon nombre, la partie est finie et il a gagné</li>
    
</ol>
<br>Voici une petite image du rendu :
<center>
    <img class="responsive-img" src="images/technologies/jeu_devinette_nombre_python/resultats.PNG" alt="Résultats attendus">
</center>
<br>
Essayez de trouver la solution sans les indices. Bonne chance !
<br>
<br>
   <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">lightbulb_outline</i>Indice 1</div>
      <div class="collapsible-body"><span>
          Pour obtenir un nombre aléatoire en Python, il faut importer le module <b>random</b> puis utiliser la fonction <b>randint(a,b)</b> qui génère un entier aléatoire entre a et b.
          <br>
          <code>
              import random<br>
              nombre_aléatoire = random.randint(0, 100)<br>
          </code>
          </span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">lightbulb_outline</i>Indice 2</div>
      <div class="collapsible-body"><span>
          Une <b>boucle</b> <i>"tant que"</i> en python s'écrit de la manière suivante :
          <br>
          
                while condition_est_vrai:
                    action
<br>
Voici un exemple de <b>condition</b> <i>a>5</i>.</span>
        </div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">lightbulb_outline</i>Indice 3</div>
      <div class="collapsible-body"><span>
          Pour <b>récupérer une valeur rentrée par l'utilisateur</b> en python et la stocker dans une variable, on écrit  :
          <br>
          
                variable = input('Entrer une valeur: ')
<br>
La valeur récupérée est une chaîne de caractères, il faut donc <b>la convertir en entier</b> grâce à <i>int(variable)</i>.</span>
        </div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">lightbulb_outline</i>Indice 4</div>
      <div class="collapsible-body"><span>
          Une <b>condition</b> en python s'écrit de la manière suivante :
          <br>
          
                if condition_est_vrai:
                    action
                elif condition_est_vrai :
                    action
                else:
                    action
</span>
        </div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">lightbulb_outline</i>Indice 5</div>
      <div class="collapsible-body"><span>
          Pour <b>afficher une valeur</b> en python on écrit :
          <br>
          
                print(valeur)
</span>
        </div>
    </li>    
    <li>
      <div class="collapsible-header"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body"><span>
          <center>
                <img class="responsive-img" src="images/technologies/jeu_devinette_nombre_python/solution.png" alt="Solution">
          </center>
          </span></div>
    </li>
  </ul>
  



