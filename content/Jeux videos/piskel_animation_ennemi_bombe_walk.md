Title: Piskel, notre personnage marche
Date: 2018-11-03 23:00
Tags: jeux vidéos, ennemie, bombe, piskel
Category: Jeux Videos
Authors: Victor Lohézic
Summary: Apprenons à faire marcher notre personnage avec le logiciel Piskel.
Image: images/jeux_videos/piskel_animation_ennemi_bombe_walk/thumbnail.png
Status: draft

<script>

  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
    </script>

<b>Vous allez apprendre à faire marcher un personnage avec [Piskel un logiciel libre de Pixel Art.](piskel-un-editeur-de-sprites.html)</b>
<br>
Si vous ne maîtrisez pas ce logiciel, je vous invite à lire [cet article](creer-son-smiley-avec-piskel.html). Nous allons reprendre le personnage [d'un article précédent.](piskel-creation-dun-personnage-de-jeux-video.html) Voici, une animation déjà réalisée : [ici](piskel-animation-dun-personnage.html) 

<br>

Tout d'abord, analysons notre façon de marcher :


<br>
<center>
    <img class="responsive-img" src="images/jeux_videos/piskel_animation_ennemi_bombe_walk/walk_cycle.png" alt="Personnage qui marche" width="50%">
</center>
<br>

Notre personnage n'ayant pas de bras, on peut se concentrer seulement sur les jambes :

<br>
<center>
    <img class="responsive-img" src="images/jeux_videos/piskel_animation_ennemi_bombe_walk/walk_cycle2.png" alt="Jambes qui marchent " width="50%">
</center>
<br>

On observe que la jambe avant recule progressivement tandis que la jambe arrière avance progressivement en montant. Au milieu les deux jambes se croisent et se superposent. Il ne reste plus qu'à faire de même avec notre personnage. Cette fois-ci, il n'y a pas de solution miracle. Il faut tester jusqu'à ce que vous soyez satisfait. Je vous invite donc à essayer par vous même puis à regarder ce que je propose, ça peut vous donner des idées. 

Voici une petite astuce (allez à 2min30) :
<br>
<div class="video-container">
    <iframe width="729" height="410" src="https://www.youtube.com/embed/y-dOHbswKLQ?t=154" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"   allowfullscreen></iframe>
</div>
<br>

   <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">vpn_key</i>Ma réalisation</div>
      <div class="collapsible-body">
          Voici toutes les images à la suite qui composent l'animation :
          <br>
          <center>
                <img class="responsive-img" src="images/jeux_videos/piskel_animation_ennemi_bombe_walk/bombe_walk.png" alt="décomposition de la marche de notre personnage.">
          </center>
           Voici l'animation :
          <center>
                <img class="responsive-img" src="images/jeux_videos/piskel_animation_ennemi_bombe_walk/bombe_walk.gif" alt="Notre personnage marche">
          </center>
          </div>
    </li>
  </ul>
  
  <br>
  J'espère que cet article vous a été utile. Un conseil, pour vos animations partez toujours d'une image, d'une vidéo qui décompose le mouvement pour savoir le dessiner ensuite.