Title: Piskel, création d'un personnage de jeux vidéo
Date: 2018-11-03 23:00
Tags: jeux vidéos, ennemie, bombe, piskel
Category: Jeux Videos
Authors: Victor Lohézic
Summary: Piskel permet de créer ses ressources graphiques dans la cadre du développement d'un jeu vidéo. Apprenons donc à dessiner avec cet outil un petit ennemi. 
Image: images/jeux_videos/piskel_creation_ennemi_bombe/thumbnail.png
Status: draft

<script>

  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
    </script>


<b>Vous allez apprendre un personnage avec [Piskel un logiciel libre de Pixel Art.](piskel-un-editeur-de-sprites.html)</b>
<br>
Si vous ne maîtrisez pas ce logiciel, je vous invite à lire [cet article](creer-son-smiley-avec-piskel.html). Cette fois-ci, pas besoin de faire de croquis car je vous propose de réaliser le personnage suivant. 

<br>
<center>
    <img class="responsive-img" src="images/jeux_videos/piskel_creation_ennemi_bombe/bombe-5.png.png" alt="Image final du personnage en forme de bombe" width="50%">
</center>
<br>


Je vous propose des indices mais essayez de le faire seul, je suis sûr que vous en avez la capacité !

   <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">lightbulb_outline</i>Indice 1</div>
      <div class="collapsible-body">
          Commencez par faire un cercle.
          <br>
          <br>
            <center>
                <img class="responsive-img" src="images/jeux_videos/piskel_creation_ennemi_bombe/bombe-1.png.png" alt="Cercle" width="50%">
            </center>
            <br>
        </div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">lightbulb_outline</i>Indice 2</div>
      <div class="collapsible-body">
          Vous pouvez ajouter les yeux.
          <br>
            <center>
                <img class="responsive-img" src="images/jeux_videos/piskel_creation_ennemi_bombe/bombe-2.png.png" alt="Cercle avec des yeux" width="50%">
            </center>
            <br>
        </div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">lightbulb_outline</i>Indice 3</div>
      <div class="collapsible-body">
          Avec sa mèche, notre personnage est magnifique.
          <br>
            <center>
                <img class="responsive-img" src="images/jeux_videos/piskel_creation_ennemi_bombe/bombe-3.png.png" alt="Ajout de la mèche à l'image précédente" width="50%">
            </center>
            <br>
        </div>
    </li>
        <li>
      <div class="collapsible-header"><i class="material-icons">lightbulb_outline</i>Indice 4</div>
      <div class="collapsible-body">
          Dessinons des pieds à notre bombe.
          <br>
            <center>
                <img class="responsive-img" src="images/jeux_videos/piskel_creation_ennemi_bombe/bombe-4.png.png" alt="Ajout de pieds à l'image précédente" width="50%">
            </center>
            <br>
        </div>
    </li>
        <li>
      <div class="collapsible-header"><i class="material-icons">lightbulb_outline</i>Indice 5</div>
      <div class="collapsible-body">
          En couleur, c'est mieux !
          <br>
            <center>
                <img class="responsive-img" src="images/jeux_videos/piskel_creation_ennemi_bombe/bombe-5.png.png" alt="Image en couleur du personnage en forme de ombe" width="50%">
            </center>
            <br>
        </div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body"><span>
          Voici un petit récapitualatif des étapes, évidemment il y a plusieurs manières d'obtenir le même résultat.
          <br>
          <center>
                <img class="responsive-img" src="images/jeux_videos/piskel_creation_ennemi_bombe/bombe.gif" alt="Solution">
          </center>
          <br>
          <center>
                <img class="responsive-img" src="images/jeux_videos/piskel_creation_ennemi_bombe/bombe.png" alt="Solution">
          </center>
          </span></div>
    </li>
  </ul>
  
  <br>
  J'espère que cet article vous a plu dans un prochain article, nous animerons notre personnage.