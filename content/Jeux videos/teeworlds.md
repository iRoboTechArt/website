Title: Teeworlds, un jeu de tir multijoueur 
Date: 2019-01-04 22:00
Tags: jeux vidéos, TeeWorlds
Category: Jeux Videos
Authors: Victor Lohézic
Summary: TeeWorlds est un jeu de tir multijoueur. Ce jeu retro est libre et gratuit !
Image: images/jeux_videos/teeworlds/thumbnail.png
Status: published

<b>Teeworlds est un jeu en 2D [libre](https://github.com/teeworlds/teeworlds) multijoueur en ligne.</b> Ce jeu multiplatforme permet jusqu'à 16 joueurs de s'affronter dans des modes de jeu variés. Le joueur incarne un personnage appelé <b>Tee</b> :
<br>
<center>
    <img class="responsive-img" src="images/jeux_videos/teeworlds/teeworlds.png" alt="Tee" width="25%">
</center>   
<br>
Très agile avec son grapin, le Tee parcourt mur et plafond. Armé d'un maillet et d'un pistolet, il se confrontera à d'autres Tee sur différents serveurs. 
Il est même possible de <b>créer ses propres maps (cartes) de jeu</b>. Découvrez sans plus attendre le trailer :
<br>
<br>
<div class="video-container">
    <iframe width="560" height="360" src="https://www.youtube.com/embed/HIF9B-4CWj8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<br>
<b><u>Les modes de jeux officiels :</u></b><br>
<b>Team deathmatch (TDM)</b> est un mode de jeu dans lequel deux équipes s'affrontent : les rouges contre les bleus. Le but est d'éliminer un maximum d'adversaires.
<b>Capture the flag (CTF)</b> se joue aussi en équipe. Le but cette fois-ci est de capturer le drapeau adverse et de le ramener dans son camp.
<br>
D'autres modes de jeux ont été développés par la communauté que vous pouvez découvrir [ici](https://fr.wikipedia.org/wiki/Teeworlds).
<br>
<center>
    <img class="responsive-img" src="images/jeux_videos/teeworlds/screenshot_desert.png" alt="Carte du désert" width="50%">
</center> 
<br>
Sur le site du jeu est proposé un [forum](https://www.teeworlds.com/forum/). Il nous permet d'être au courant de l'actualité du jeu, d'échanger avec la communauté sur la création de maps, du design du jeu. On peut aussi y contribuer en partageant les bugs rencontrés. Des tutoriaux y sont aussi proposés.

Si le jeu vous intéresse, vous pouvez <b>le télécharger gratuitement</b> [ici](https://www.teeworlds.com/?page=downloads).

Pour en savoir plus, il existe <b>un fan site en français</b> qui dans un article explique comment créer un [serveur](https://www.teeworlds.fr/index.php?f_id_contenu=858&f_id_type=95).