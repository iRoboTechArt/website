Title: Niveau 2 et 3 du jeu Roberto
Date: 2018-12-28 22:45
Tags: jeux vidéos, robot
Category: Jeux Videos
Authors: Victor Lohézic
Summary: Jean-Yanis développe durant son temps libre un jeu vidéo. Le jeu comporte désormais 3 niveaux.
Image: images/jeux_videos/roberto_le_petit_robot_niveau_2_et_3/thumbnail.png
Status: published

<script>

  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
    </script>
<b>Jean-Yanis développe durant son temps libre un jeu vidéo. Le jeu comporte désormais 3 niveaux.</b>  <br>
<link rel="shortcut icon" href="TemplateData/favicon.ico">
<link rel="stylesheet" href="images/jeux_videos/roberto_le_petit_robot_niveau_2_et_3/TemplateData/style.css">
<script src="images/jeux_videos/roberto_le_petit_robot_niveau_2_et_3/TemplateData/UnityProgress.js"></script>  
<script src="images/jeux_videos/roberto_le_petit_robot_niveau_2_et_3/Build/UnityLoader.js"></script>
<script>
    var gameInstance = UnityLoader.instantiate("gameContainer", "images/jeux_videos/roberto_le_petit_robot_niveau_2_et_3/Build/Jeu Internet.json", {onProgress: UnityProgress});
</script>


Cet élève de troisième membre de l'association est en train de programmer <b>un jeu de platforme 2D</b> avec le moteur de jeu qui s'appelle <b>Unity</b>. Si vous voulez en savoir plus sur son jeu et lui, je vous invite à aller voir [cet article](roberto-le-petit-robot.html). Et pour jouer au niveau 1, c'est [ici](roberto-le-petit-robot.html). 

<br>
Depuis ce dernier article, <b>deux niveaux</b> ont été ajoutés au jeu <i>"Roberto le petit robot"</i>. Parviendrez-vous à venir à bout de cette version ? 
<br>
Pour que votre expérience soit la meilleure, il est conseillé de jouer en grand écran. Il se peut que le jeu ne fonctionne pas sur certains navigateurs, nous vous conseillons : <b>Mozilla Firefox</b>.
<br>
<br>


<div class="webgl-content">
      <div id="gameContainer" style="width: 100%; height:100%;"></div>
      <div class="footer">
        <div class="webgl-logo"></div>
        <div class="fullscreen" onclick="gameInstance.SetFullscreen(1)"></div>
      </div>
    </div>
   <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">info</i>Comment jouer ?</div>
      <div class="collapsible-body"><span>
         <table class="responsive-table">
        <thead>
          <tr>
              <th>Touches</th>
              <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>flèche de gauche ou a</td>
            <td>déplacement de Roberto vers la gauche</td>
          </tr>
          <tr>
            <td>flèche de droite ou d</td>
            <td>déplacement de Roberto vers la droite</td>
          </tr>
          <tr>
            <td>barre d'espace</td>
            <td>sauter</td>
          </tr>
        </tbody>
      </table>
          </span></div>
    </li>