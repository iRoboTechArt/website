Title: Piskel, animation d'un personnage
Date: 2018-11-03 23:00
Tags: jeux vidéos, ennemie, bombe, piskel
Category: Jeux Videos
Authors: Victor Lohézic
Summary: Apprenons à animer un personnage dans Piskel.L'animation que nous allons réaliser s'appelle idle.
Image: images/jeux_videos/piskel_animation_ennemi_bombe_idle/thumbnail.png
Status: draft

<b>Vous allez apprendre à animer un personnage avec [Piskel un logiciel libre de Pixel Art.](piskel-un-editeur-de-sprites.html)</b>
<br>
Si vous ne maîtrisez pas ce logiciel, je vous invite à lire [cet article](creer-son-smiley-avec-piskel.html). Nous allons reprendre le personnage [du dernier article.](piskel-creation-dun-personnage-de-jeux-video.html) Nous allons réaliser l'animation idle de notre personnage. Cette animation sera utilisée dans un jeu vidéo lorsque notre personnage ne fera rien, ne bougera pas. Voici une petit vidéo avec des animations assez marrantes : 
<br>
<div class="video-container">
    <iframe width="642" height="361" src="https://www.youtube.com/embed/1pVaVyWGkpM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<br>
Nous nous allons faire une animation simple qui va correspondre à sa respiration. Et oui, notre bombe respire ^^.

C'est très simple à réaliser, il suffit par exemple de descendre le corps de la bombe ou de racourcir les pieds.

En image, ce sera plus clair :

<br>
<center>
    <img class="responsive-img" src="images/jeux_videos/piskel_animation_ennemi_bombe_idle/bombe_idle-1.png.png" alt="L'image de notre personnage, rien n'a changé" width="50%">
</center>
<br>

<br>
<center>
    <img class="responsive-img" src="images/jeux_videos/piskel_animation_ennemi_bombe_idle/bombe_idle-2.png.png" alt="Notre personnage avec les pieds raccourci" width="50%">
</center>
<br>

<br>
<center>
    <img class="responsive-img" src="images/jeux_videos/piskel_animation_ennemi_bombe_idle/bombe_idle.png" alt="Les deux images précédentes à la suite" width="50%">
</center>
<br>

Voici le résultat en GIF :

<br>
<center>
    <img class="responsive-img" src="images/jeux_videos/piskel_animation_ennemi_bombe_idle/bombe_idle.gif" alt="Animation de notre personnage" width="50%">
</center>
<br>

J'espère que cet article vous a plu dans un prochain article, nous ferons marcher notre personnage.