Title: Roberto le petit robot
Date: 2018-11-03 23:00
Tags: jeux vidéos, robot
Category: Jeux Videos
Authors: Victor Lohézic
Summary: Jean-Yanis développe durant son temps libre un jeu vidéo. Il vous propose de tester le premier niveau.
Image: images/jeux_videos/roberto_le_petit_robot_niveau_1/thumbnail.png
Status: published

<script>

  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
    </script>
<b>Jean-Yanis développe durant son temps libre un jeu vidéo. Il vous propose de tester le premier niveau.</b>  <br>
<link rel="shortcut icon" href="TemplateData/favicon.ico">
<link rel="stylesheet" href="images/jeux_videos/roberto_le_petit_robot_niveau_1/TemplateData/style.css">
<script src="images/jeux_videos/roberto_le_petit_robot_niveau_1/TemplateData/UnityProgress.js"></script>  
<script src="images/jeux_videos/roberto_le_petit_robot_niveau_1/Build/UnityLoader.js"></script>
<script>
    var gameInstance = UnityLoader.instantiate("gameContainer", "images/jeux_videos/roberto_le_petit_robot_niveau_1/Build/Web.json", {onProgress: UnityProgress});
</script>


Cet élève de troisième membre de l'association est en train de programmer <b>un jeu de platforme 2D</b>. Il l'a appelé <i>Roberto le petit robot</i>. Il utilise un moteur de jeu qui s'appelle <b>Unity</b>. Ses scripts sont en C#. Il a appris ce langage au fil de ses lectures sur les forums Unity. 
<br>
<div class="video-container">
    <iframe width="642" height="361" src="https://www.youtube.com/embed/AJ6Mkx1KEns" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
<br>
<b>L'objectif de Roberto est de parcourir des niveaux afin d'atteindre un boss final.</b> Ainsi après avoir écrit une centaine de lignes de code, Jean-Yanis vous demande d'aider Roberto à atteindre le drapeau vert correspondant à la fin du premier niveau.
<br>
<br>
<b>Amusez-vous bien</b> avec sa démo (le sytème de pièce n'est pas encore fonctionnel) ! Pour que votre expérience soit la meilleure, il est conseillé de jouer en grand écran. Il se peut que le jeu ne fonctionne pas sur certains navigateurs, nous vous conseillons : <b>Mozilla Firefox</b>.
<br>
<br>


<div class="webgl-content">
      <div id="gameContainer" style="width: 100%; height:100%;"></div>
      <div class="footer">
        <div class="webgl-logo"></div>
        <div class="fullscreen" onclick="gameInstance.SetFullscreen(1)"></div>
      </div>
    </div>
   <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">info</i>Comment jouer ?</div>
      <div class="collapsible-body"><span>
         <table class="responsive-table">
        <thead>
          <tr>
              <th>Touches</th>
              <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>flèche de gauche ou a</td>
            <td>déplacement de Roberto vers la gauche</td>
          </tr>
          <tr>
            <td>flèche de droite ou d</td>
            <td>déplacement de Roberto vers la droite</td>
          </tr>
          <tr>
            <td>barre d'espace</td>
            <td>sauter</td>
          </tr>
        </tbody>
      </table>
          </span></div>
    </li>
</ul>
Trop facile ? Voici, les deux niveaux qui suivent [ici](niveau-2-et-3-du-jeu-roberto.html).