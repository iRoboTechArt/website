Title: Blague de programmeurs #4
Date: 2018-12-16 10:55
Tags: blague, iRoboTechArt
Category: Humour
Authors: Victor Lohézic
Summary: Voici une petite image trouvée sur le web destinée aux programmeurs.
Image: images/humour/1blague_de_programmeurs/thumbnail.png
Status: draft

Voici une petite image trouvée sur le site [commitstrip](http://www.commitstrip.com/fr/2017/04/03/its-not-a-bug/) destinée aux programmeurs.

<center>
    <img class="responsive-img" src="https://www.commitstrip.com/wp-content/uploads/2017/04/Strip-Jeu-du-solitaire-650-final.gif" width="80%" alt="Image drôle">
</center>
