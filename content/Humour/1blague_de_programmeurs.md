Title: Blague de programmeurs #1
Date: 2018-11-10 12:00
Tags: blague, iRoboTechArt
Category: Humour
Authors: Victor Lohézic
Summary: Voici une petite image trouvée sur le web destinée aux programmeurs.
Image: images/humour/1blague_de_programmeurs/thumbnail.png
Status: published

Voici une petite image trouvée sur le web destinée aux programmeurs.

<center>
    <img class="responsive-img" src="images/humour/1blague_de_programmeurs/blague.jpg" width="50%" alt="Image drôle">
</center>
