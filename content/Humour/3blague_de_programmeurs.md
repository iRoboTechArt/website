Title: Blague de programmeurs #3
Date: 2018-12-01 23:00
Tags: blague, iRoboTechArt
Category: Humour
Authors: Victor Lohézic
Summary: Voici une petite image trouvée sur le web destinée aux programmeurs.
Image: images/humour/1blague_de_programmeurs/thumbnail.png
Status: published

Voici une petite image trouvée sur ce [site](https://devrant.com/rants/1287925/found-this-in-a-telegram-channel) destinée aux programmeurs.

<center>
    <img class="responsive-img" src="https://img.devrant.com/devrant/rant/r_1287925_cesRL.jpg" width="80%" alt="Image drôle">
</center>
