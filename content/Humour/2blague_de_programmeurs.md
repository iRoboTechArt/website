Title: Blague de programmeurs #2
Date: 2018-11-15 12:55
Tags: blague, iRoboTechArt
Category: Humour
Authors: Victor Lohézic
Summary: Voici une petite image trouvée sur le web destinée aux programmeurs.
Image: images/humour/1blague_de_programmeurs/thumbnail.png
Status: published

Voici une petite image trouvée sur le site [commitstrip](http://www.commitstrip.com/fr/2018/05/09/progress/) destinée aux programmeurs.

<center>
    <img class="responsive-img" src="https://www.commitstrip.com/wp-content/uploads/2018/05/Strip-La-joie-du-message-derreur-650-final.jpg" width="80%" alt="Image drôle">
</center>
