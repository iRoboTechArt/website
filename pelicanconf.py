#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Victor'
SITENAME = 'iRoboTechArt'
SITEURL = ''

#Thème
THEME = 'irobotechart'
PATH = 'content'
STATIC_PATHS = ['images']

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 15


# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

TEMPLATE_PAGES  =  { 'contact.html' :  'contact.html',
                     'informations.html' :  'informations.html',
                     'labs/designlab.html' :  'labs/designlab.html',
                     'labs/educlab.html' :  'labs/educlab.html',
                     'labs/explolab.html' :  'labs/explolab.html',
                     'labs/fablab.html' :  'labs/fablab.html',
                     'labs/livinglab.html' :  'labs/livinglab.html',
                     'labs/medialab.html' :  'labs/medialab.html',
                     'live.html' :  'live.html',
                     'equipe_irobotechart.html' :  'equipe_irobotechart.html'
                   }

PLUGIN_PATHS = ["plugins"]

PLUGINS = ["sitemap"]

SITEMAP = {
    'format': 'xml'
}

